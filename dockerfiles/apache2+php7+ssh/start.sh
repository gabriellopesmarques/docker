#!/bin/bash
set -e

echo "hello from docker" > /var/www/html/index.html

# start all the services
/usr/local/bin/supervisord -n
