# Apache
- Debian (latest)
- Apache (latest)

## Build image
```
docker build -t gl-apache:latest .
```

## Create and run a container
```
docker run --name webserver -p 80:80 -v "$(pwd):/var/www/html" -d gl-apache
```
