# debian+python+flask (DPF)

## build
```
docker build -t flask:latest .
```

## run
```
docker run -p 80:5000 -v "$(pwd):/app" -d flask
```

