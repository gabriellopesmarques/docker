# LA_P 

DOCUMENT_ROOT=/var/www/html/public/

- Debian (latest)
- Apache (latest)

## Build image
```
docker build -t gl-php73-dr-public:latest .
```

## Create and run a container
```
docker run --name webserver -p 80:80 -v "$(pwd):/var/www/html/" -d gl-php73-dr-public
```

