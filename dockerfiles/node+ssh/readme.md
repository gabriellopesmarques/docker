# node+ssh

## build
```
 docker build -t node_ssh .
```

## run
```
docker run -p 80:80 node_ssh
```

set volume
```
docker run -p 80:80 -v "$(pwd):/app/" -d node_ssh
```


```
ssh root@name_of_container
password: toor
```