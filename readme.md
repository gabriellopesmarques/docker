# Docker 🐋

Docker é uma coleção de ferramentas que traz como principal vantagem a containização, permitindo que criemos containers de forma isolada, o que nos permite rodar diversas aplicações ao mesmo tempo sem conflitos.

Para criarmos um container precisamos de uma imagem, chamamos imagem pois é como se fosse uma "foto", capturando as configurações, bibliotecas e etc, da máquina.

Você pode criar suas proprias imagens ou utilizar imagens já existentes. Existem diversas imagens já prontas com as principais ferramentas do mercado no, [docker hub, o repositório oficial do docker](https://hub.docker.com)


# Criando Containers

Para criar um container basta "rodar" uma imagem, como por exemplo:
```
docker run hello-world
```

O docker irá criar um container apartir da imagem hello-world, se você não tiver essa imagem local o docker irá baixa-la do [docker hub](https://hub.docker.com). E assim que executar a menssagem de "hello world" ele encerrará o container.


Para verificar containers em execução podemos utilizar o comando:
```
docker ps
```

e para ver todos os containers, o comando:
```
docker ps -a
```


## Opções
Temos diversas opções para criar um container, para ver as todas as possibilidades executamos o comando:
```
docker run --help
```

## Exemplos

Podemos executar o comando ```echo "hello world"``` num container baseado na imagem do ubuntu
```
docker run ubuntu echo "hello world"
```

**interativo**
Cria um container e nos permite interagir com o terminal do ubuntu
```
docker run -it ubuntu /bin/bash
```

**background (detach)**
o parametro -d, executa o container em background (não bloqueia o terminal)
```
docker run -d {image-name}
```

**portas**
o parametro -p, atribui as portas do container para portas do host
```
docker run -d -p 12345:80 {image-name}
```

**nome**
o parametro --name, nomeia o container
```
docker run --name ubuntu-18_04 ubuntu:bionic
```

**environment**
o parametro -e, define variaveis de ambiente
```
docker run --name mariadb -p 3306:3306 -e MYSQL_ROOT_PASSWORD=my-secret-pw -d mariadb:10.4.7-bionic
```

**volumes**
o parametro -v, cria um volume no container com um diretório de referência no host
```
docker run -p 80:80 -v "$(pwd):/usr/share/nginx/html" -d nginx
```


### ações básicas

parar um container em execução
```
docker stop {container-id or container-name}
```

roda um container existente
```
docker start {container-id or container-name}
```

deletar um container
```
docker rm {container-id or container-name}
```

remove todos containers inativos
```
docker container prune
```


## Images

criar imagens apartir de Dockerfiles:
```
docker build -t <imageName>:<version(latest by default)> path/where/Dockerfile/is
```

apenas baixar uma imagem
```
docker pull <image>
```


# Snippets

## static webserver with nginx
```
docker run --name static-webserver -p 80:80 -v "$(pwd):/usr/share/nginx/html" -d nginx
```

## spa with nginx
```
docker run -d -v ${PWD}:/usr/src/app -p 80:80 desmart/nginx-spa
```

## mongodb
```
docker run --name mongodb -d -p 27017:27017 mongo
```

## mariadb
```
docker run --name mariadb -p 3306:3306 -e MYSQL_DATABASE=database -e MYSQL_ROOT_PASSWORD=toor -d mariadb:10.4.7-bionic
```

## mysql
```
docker run --name mysql57 -p 3306:3306 -e MYSQL_DATABASE=database -e MYSQL_ROOT_PASSWORD=toor -d mysql:5.7
```

### Backup
```
docker exec CONTAINER /usr/bin/mysqldump -u root --password=root DATABASE > backup.sql
```

### Restore
```
cat backup.sql | docker exec -i CONTAINER /usr/bin/mysql -u root --password=root DATABASE
```

### MySQL backup official docker docs 
```
docker exec some-mysql sh -c 'exec mysqldump --all-databases -uroot -p"$MYSQL_ROOT_PASSWORD"' > /some/path/on/your/host/all-databases.sql
```

### MySQL import official docker docs
```
docker exec -i some-mysql sh -c 'exec mysql -uroot -p"$MYSQL_ROOT_PASSWORD"' < /some/path/on/your/host/all-databases.sql
```

## node app
```
docker run -d -p 8080:3000 -v "$(pwd):/var/www" -w "/var/www" node npm start
```

## php7 + apache
```
docker run --name "$(basename $(pwd))_php_apache" -p 80:80 -v "$(pwd):/var/www/html/" -d php:7-apache
```

## php7.2 + apache + htaccess enabled + common extensions
```
docker run --name "$(basename $(pwd))_chialab" -p 80:80 -v "$PWD":/var/www/html/ -d chialab/php:7.2-apache
```


# Network

connect containers in creation

```
docker network create lamp-network
docker run --name test_webserver -p 80:80 -v "$(pwd):/var/www/html" --network lamp-network -d gl-php73-dr-public
docker run --name test_mariadb -p 3306:3306 -e MYSQL_ROOT_PASSWORD=toor --network lamp-network -d mariadb:10.4.7-bionic
```

To connect a running container to an existing user-defined bridge
```
docker network connect my-net my-nginx
```

remove network
```
docker network rm my-net
```

## important!
- Dockerfile create images
- Dockercomposer create containers

